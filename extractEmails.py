import sys

target_file = sys.argv[1]

def file_to_list(words_file):
    return [word for line in open(words_file, 'r') for word in line.split()]

def file_to_str(words_file):
    output=""
    for line in open(words_file, 'r'):
        output+=line
    return output

def get_emails_as_str(word_list):
    output = ""
    for word in word_list:
        if is_email(word):
            output+=word+"\n"
    return output[:len(output)-1]

def get_emails_as_list(word_list):
    output = []
    for word in word_list:
        if is_email(word):
            output.append(word)
    return output

# TODO make more sophisticated
def is_email(word):
    return "@" in word

# For testing
# print(get_emails_as_str(file_to_list(target_file)))
