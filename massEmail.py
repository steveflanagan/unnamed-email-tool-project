from extractEmails import *
import sys, subprocess, os


# TODO explore whether theres a way to generate email natively in python
# rather than via subprocess calls

# TODO support windows, see if osx and linux calls are same
# TODO somehow make this send with a from different from x@macbookpro.local
# TODO is there a way to make this email avoid the spam folder?
def send_email_osx(email_list, subject, content):
    for email in email_list:
        command = "mail -s " + subject + " " + email + " <<< " + content
        subprocess.Popen(command, shell=True)

# main
email_file = sys.argv[1]
email_list = get_emails_as_list(file_to_list(email_file))
if os.path.isfile(sys.argv[3]):
    print("Generating email from content in " + sys.argv[3])
    send_email_osx(email_list, "\"" + sys.argv[2] + "\"", "\"" + file_to_str(sys.argv[3]) + "\"")
else:
    print("Generating email with content: " + sys.argv[3])
    send_email_osx(email_list, "\"" + sys.argv[2] + "\"", "\"" + sys.argv[3] + "\"")
