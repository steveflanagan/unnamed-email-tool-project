# Unnamed Email Tool Suite

A suite of tools for managing an email list, blasting messages out, and all sorts of other neat things

Most tools today are:

- Online exclusive, controlled by some other company, instead of in your control on your own laptop

- Pushing you to adopt features you don't want

- Operating on a freemium model that keeps all the good stuff behind a paywall

We're changing that!

My name's Steve Flanagan. I manage email lists for clubs, for business, and for newsletters. The tools available to do this were shitty. So I built my own - I hope you enjoy them.

## Usage

1. Clone this repository

2. Put your list of emails, in whatever format, with any cluttering text you don't feel like removing, in a text file - can be as simple as just list.txt

    1. Optionally, put the body content of your planned email in another file, also named whatever you want - such as content.txt

3. In your terminal, run

    > python3 massEmail.py /path/to/list.txt \"subject\" \"the body content\"

    for a short email, or

    > python3 massEmail.py /path/to/list.txt \"subject\" /path/to/content.txt

    if you put your email's body content in a separate file

4. Alternatively, to simply output a list of found emails in a text file, uncomment the last line in extractEmails.py and run:

    > python3 extractEmails.py /path/to/list.txt

    ...a more user-friendly way to do simple extraction is coming soon :)

### Current features:

- Detect emails within an arbitrarily cluttered file, including spreadsheets, web pages, etc. Receive output in another file, as a string, or as a list

- Send an email out to all emails detected within an arbitrary file, using the above - no more manually copying and pasting from Google form responses!

### Planned features:

- Spam box evasion

- Webpage scraping (ie a Twitter feed) to gather emails

- Advanced email detection - ie, finding emails written to avoid bots, such as name (at) emailprovider.com

- Aesthetically pleasing mass emails

- Tools for linking social media campaigns to email campaigns

- Statistics on your conversion rates and the effectiveness of your emailing

- A nice old GUI
